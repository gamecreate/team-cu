int N = 2;
int val[2];
float data[2];
int Data[2];

void setup(){
  Serial.begin(115200);
  Serial.println("Start");
}

void loop(){
  
  val[0] = IDSread(0);
  val[1] = IDSread(1);
  
  for(int i = 0; i < N ; i++ ){
    data[i] =  (5 *float(val[i]) / 1024) ;
    Data[i] = int(26.543 * pow(data[i],-1.2091));  
   
    if (Data[i] > 60){
      Data[i] = 60;
    }else if(Data[i] <5){
      Data[i] = 5;
    }
  }
  Serial.print(Data[0]);
  Serial.print(",");
  Serial.println(Data[1]);
  delay(50);
}

int IDSread(int PinNo){
  long ans;
  int i;
  ans = 0;
  for(i=0;i<100;i++){
    ans = ans + analogRead(PinNo);
    //指定のアナログピン（0番端子）から読み取り
  }
  return ans/100;
  //100回の平均値を返します
}
