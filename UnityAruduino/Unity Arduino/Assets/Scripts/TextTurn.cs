﻿using UnityEngine;
using System.Collections;

public class TextTurn : MonoBehaviour {

	private float timer ;
	private int count;
	// Use this for initialization
	void Start () {
		timer = 0;
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;
		if(timer > 0.5f && count == 0){
			this.guiText.enabled = !this.guiText.enabled;
			count = 1;
		}
		if(timer > 1.0f && count == 1){
			this.guiText.enabled = !this.guiText.enabled;
			count = 0;
			timer = 0;
		}
	}
}
