﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float speed;
	//public float shotDelay;
	public GameObject bullet;
	public GameObject explosion_Prefabs;

	public bool canShot;

	private float timer = 0;
	private GameObject bullet_clone;
	private Transform shotPosition;

	void Shot(Transform origin){
		bullet_clone = Instantiate(bullet,origin.transform.position,origin.transform.rotation) as GameObject;
	}
	void Move(Vector2 direction){
		this.rigidbody2D.velocity = direction * speed;
	}

	void Explosion(){
		Instantiate(explosion_Prefabs,transform.position,transform.rotation);
	}

	void OnTriggerEnter2D(Collider2D c){
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		if( layerName == "Bullet(Player)"){
			Explosion();
			Destroy(c.gameObject);
			Destroy(this.gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		shotPosition = transform.GetChild(0);
		Move(transform.up * -1);
	}
	
	// Update is called once per frame
	void Update () {
		if(canShot){
			timer += Time.deltaTime;
			if(timer > 2.0f){
				Shot(shotPosition);
				timer = 0;
			}
		}
	}
}
