﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public GameObject explosion;
	public GameObject game;
	GameManager gameManager;

	void Start(){
		gameManager = game.GetComponent<GameManager>();
	}


	void Explosion(){
		Instantiate(explosion,transform.position,transform.rotation);
	}

	// ぶつかった瞬間に呼び出される
	void OnTriggerEnter2D (Collider2D c)
	{
		// レイヤー名を取得
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		
		// レイヤー名がBullet (Enemy)またはEnemyの場合は爆発
		if( layerName == "Bullet(Enemy)" || layerName == "Enemy")
		{	
			// プレイヤーを削除
			Explosion();
			Destroy(c.gameObject);
			gameManager.setFlag();
			Debug.Log("gameend");
			Destroy (gameObject);
		}
	}
}
