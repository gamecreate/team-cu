﻿using UnityEngine;
using System.Collections;

public class TitleEnemy : MonoBehaviour {

	public GameObject explosion_Prefabs;
	Title title;

	void Explosion(){
		Instantiate(explosion_Prefabs,transform.position,transform.rotation);
	}
	
	void OnTriggerEnter2D(Collider2D c){
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		if( layerName == "Bullet(Player)"){
			Explosion();
			title.AddCount();
			Destroy(c.gameObject);
			Destroy(this.gameObject);
		}
	}
	
	void Start () {
		title = GameObject.Find("BackGround_Prefabs").GetComponent<Title>();
	}
}